# Laravel Mesibo API

[![Latest Stable Version](https://poser.pugx.org/rkcreative/laravel-mesibo-api/v)](//packagist.org/packages/rkcreative/laravel-mesibo-api)
[![Total Downloads](https://poser.pugx.org/rkcreative/laravel-mesibo-api/downloads)](//packagist.org/packages/rkcreative/laravel-mesibo-api)
[![Latest Unstable Version](https://poser.pugx.org/rkcreative/laravel-mesibo-api/v/unstable)](//packagist.org/packages/rkcreative/laravel-mesibo-api)
[![License](https://poser.pugx.org/rkcreative/laravel-mesibo-api/license)](//packagist.org/packages/rkcreative/laravel-mesibo-api)

Laravel package for integrating the [Mesibo Backend API](https://mesibo.com/documentation/api/backend-api/) into your
app.

## Dependencies

- [PHP](https://secure.php.net): ^7.2
- [guzzlehttp/guzzle](https://github.com/guzzle/guzzle): 5.\* | 6.\* | 7.\
- [illuminate/support](https://github.com/illuminate/support): 5.\* | 6.\* | 7.\* | 8.\*

## Requirements

You will need to create an account with [Mesibo](https://mesibo.com), create an app and obtain an app token. You will
need to define your app token and the api url in the config file. The api url will either be the default Mesibo backend
api url (https://api.mesibo.com/api.php) or your own hosted url.

## Install

You can install the package via [Composer](https://getcomposer.org/)

```bash
$ composer require rkcreative/laravel-mesibo-api
```

In Laravel 5.5 or above the service provider will automatically get registered. In older versions of the framework just
add the service provider in `config/app.php` file:

```php
'providers' => [
    ...
    /*
    * Package Service Providers...
    */
    
    RKCreative\LaravelMesiboApi\LaravelMesiboApiServiceProvider::class,   
    ...
],

'aliases' => [
    ...
    'MesiboApi'      => RKCreative\LaravelMesiboApi\LaravelMesiboApiFacade::class,
    ...
],
```

You can publish the config file with:

```bash
$ php artisan vendor:publish --provider="RKCreative\LaravelMesiboApi\LaravelMesiboApiServiceProvider" --tag=config
```

When published, [the `config/mesibo-api .php` config](config/mesibo-api.php) file contains:

```php
<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Mesibo App Token
    |--------------------------------------------------------------------------
    |
    | You can generate an app token for your app from the Mesibo dashboard
    | and enter it here
    |
    */
    'app_token' => env('MESIBO_APP_TOKEN', ''),

    /*
    |--------------------------------------------------------------------------
    | Mesibo API Url
    |--------------------------------------------------------------------------
    |
    | Add the Mesibo API url here
    |
    */
    'api_url'   => env('MESIBO_API_URL', 'https://api.mesibo.com/api.php'),

    /*
    |--------------------------------------------------------------------------
    | App ID
    |--------------------------------------------------------------------------
    |
    | Each of your apps (web/ios/android) will have a unique app id. For
    | this app, you can just use the app name define in your app config or
    | anything that will help you identify where your users are interacting
    | from. (ie, com.yourdomain.yourapp)
    |
    */
    'app_id'    => config('app.name', 'MyApp'),
];
```

## Usage

Each action below has required and optional parameters needed to return results. **You can read more
about [Mesibo API Options](https://mesibo.com/documentation/api/backend-api/#mesibo-backend-api-url).**

### Facade

---

#### Create a User

```php
Mesibo::addUser($identifier, $name = null, $parameters = [])
```

Accepts:

- identifier `(string):required` A unique identifier for the user.
- name `(string):optional` A label or name such as a username for the user.
- parameters `(array):optional` An optional array of parameters provided
  by [Mesibo](https://mesibo.com/documentation/api/backend-api/#mesibo-backend-api-url).

Returns on _success_:

- user `(object)` A user object
    - uid `(string);` A unique user id.
    - token `(string)` A unique token.

Returns on _failure_:

- false `(boolean)`

Example:

```php
$user = \Mesibo::addUser('user1@mail.com', 'User1');
$uid = $user->uid;
$token = $user->token;
```

---

#### Edit a User

```php
Mesibo::editUser($uid, $parameters = [])
```

Accepts:

- uid `(string):required` The user id returned when adding a user.
- parameters `(array):optional` An optional array of parameters provided
  by [Mesibo](https://mesibo.com/documentation/api/backend-api/#mesibo-backend-api-url).

Returns on _success_:

- true `(boolean)`

Returns on _failure_:

- false `(boolean)`

Example:

```php
\Mesibo::editUser('12345', ['active' => '0']);
```

---

#### Delete a User

```php
Mesibo::deleteUser($uid)
```

Accepts:

- uid `(string):required` The user id returned when the user was added.

Returns on _success_:

- true `(boolean)`

Returns on _failure_:

- false `(boolean)`

Example:

```php
\Mesibo::deleteUser('12345');
```

---

#### Delete a User Token

```php
Mesibo::deleteToken($uid)
```

Accepts:

- uid `(string):required` The user id returned when the user was added.

Returns on _success_:

- true `(boolean)`

Returns on _failure_:

- false `(boolean)`

Example:

```php
\Mesibo::deleteToken('12345');
```

---

#### Create a Group

```php
Mesibo::addGroup($name = null, $parameters = [])
```

Accepts:

- name `(string):optional` An optional name for the group.
- parameters `(array):optional` An optional array of parameters provided
  by [Mesibo](https://mesibo.com/documentation/api/backend-api/#mesibo-backend-api-url).

Returns on _success_:

- gid `(string)` A unique group id.

Returns on _failure_:

- `false`

Example:

```php
$gid = \Mesibo::addGroup(null, ['flag' => '0']);
```

---

#### Edit a Group

```php
Mesibo::editGroup($gid, $parameters[] = null)
```

Accepts:

- gid `(string):required` The group id returned when adding a group.
- parameters `(array):optional` An optional array of parameters provided
  by [Mesibo](https://mesibo.com/documentation/api/backend-api/#mesibo-backend-api-url).

Returns on _success_:

- true `(boolean)`

Returns on _failure_:

- false `(boolean)`

Example:

```php
\Mesibo::editGroup('12345', ['active' => '0']);
```

---

#### Add or Remove Group Members

```php
Mesibo::editGroupMembers($gid, $members, $delete = false, $parameters = [])
```

Accepts:

- gid `(string):required` The group id returned when adding a group.
- members `(string):optional` A comma seperated list of member identifiers that were used to create users.
- delete `(boolean):optional` If set to true, delete the list of members from the group. Default false.
- parameters `(array):optional` An optional array of parameters provided
  by [Mesibo](https://mesibo.com/documentation/api/backend-api/#mesibo-backend-api-url).

Returns on _success_:

- true `(boolean)`

Returns on _failure_:

- false `(boolean)`

Example:

```php
\Mesibo::editGroupMembers('12345', 'user@mail.com,user2@mail.com,user3@mail.com', false, ['cs' => '0']);
```

---

#### Delete a Group

```php
Mesibo::deleteGroup($gid)
```

Accepts:

- gid `(string):required` The group id returned when the group was added.

Returns on _success_:

- true `(boolean)`

Returns on _failure_:

- false `(boolean)`

Example:

```php
\Mesibo::deleteGroup('12345');
```

---

## Testing

``` bash
$ composer test
```

## Contributing

### Adding code

The current set of actions that this library supports is incomplete. Additional actions will be added as time permits.
If you wish to add to this library, please create a pull request.

### Security Vulnerabilities

If you discover any security-related issues, please use the issue tracker. All security vulnerabilities will be promptly
addressed.

## License

The Laravel Mesibo API package is open-source software licensed under the [MIT license](LICENSE.md).
