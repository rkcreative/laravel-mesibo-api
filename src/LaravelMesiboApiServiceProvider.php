<?php

namespace RKCreative\LaravelMesiboApi;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class LaravelMesiboApiServiceProvider extends BaseServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('mesibo_api', function () {

            return new MesiboApi();
        });
        $this->app->make(MesiboApi::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->publishes([
            __DIR__.'/config/mesibo-api.php' => config_path('mesibo-api.php'),
        ], 'config');
    }
}
