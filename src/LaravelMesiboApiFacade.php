<?php

namespace RKCreative\LaravelMesiboApi;

use Illuminate\Support\Facades\Facade;

/**
 * Class LaravelMesiboApiFacade
 *
 * @package RKCreative\LaravelMesiboApi
 */
class LaravelMesiboApiFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'mesibo_api';
    }
}
