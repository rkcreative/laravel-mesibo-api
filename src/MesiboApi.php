<?php

namespace RKCreative\LaravelMesiboApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Message;
use function json_decode;
use function mb_strtolower;

class MesiboApi
{
    private static $operation;
    private static $appToken;
    private static $apiUrl;
    private static $appId;

    /**
     * MesiboApi constructor.
     */
    public function __construct()
    {
        self::$appToken = config('mesibo-api.app_token');
        self::$apiUrl = config('mesibo-api.api_url');
        self::$appId = mb_strtolower(config('mesibo-api.app_id'));
    }

    /**
     * Add a user
     *
     * @param $identifier
     * @param  null  $name
     * @param $parameters
     *
     * @return false|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function addUser(
        $identifier,
        $name = null,
        $parameters = []
    ) {
        self::$operation = 'useradd';
        $parameters['addr'] = $identifier;
        $parameters['appid'] = self::$appId;
        $parameters['name'] = $name;

        $result = self::sendRequest($parameters);

        return $result ? $result->user : $result;
    }

    public static function editUser(
        $uid,
        $parameters = null
    ) {
        self::$operation = 'userset';
        $parameters['uid'] = $uid;
        $parameters['appid'] = self::$appId;

        return self::sendRequest($parameters);
    }

    /**
     * Delete a user
     *
     * @param $uid
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function deleteUser($uid)
    {
        self::$operation = 'userdel';
        $parameters['uid'] = $uid;
        $parameters['appid'] = self::$appId;

        return self::sendRequest($parameters);
    }

    /**
     * Delete a user access token
     *
     * @param $uid
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function deleteToken($uid)
    {

        self::$operation = 'deltoken';
        $parameters['uid'] = $uid;

        return self::sendRequest($parameters);
    }

    /**
     * Create a group
     *
     * @param $name
     * @param $parameters
     *
     * @return false|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function addGroup($name, $parameters = null)
    {
        self::$operation = 'groupadd';
        $parameters['name'] = $name;

        $result = self::sendRequest($parameters);

        return $result ? $result->group->gid : $result;
    }

    /**
     * Edit a group
     *
     * @param $groupid
     * @param  null  $parameters
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function editGroup(
        $groupid,
        $parameters = null
    ) {
        self::$operation = 'groupset';
        $parameters['gid'] = $groupid;

        return self::sendRequest($parameters);
    }

    /**
     * Edit group members
     *
     * @param $groupid
     * @param $members
     * @param  int  $delete
     * @param  null  $parameters
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function editGroupMembers(
        $groupid,
        $members,
        $delete = 0,
        $parameters = null
    ) {
        self::$operation = 'groupeditmembers';
        $parameters['gid'] = $groupid;
        $parameters['m'] = $members;
        $parameters['delete'] = $delete;

        return self::sendRequest($parameters);
    }

    /**
     * Delete a group
     *
     * @param $groupid
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function deleteGroup($groupid)
    {
        self::$operation = 'groupdel';
        $parameters = [];
        $parameters['gid'] = $groupid;

        return self::sendRequest($parameters);
    }

    /**
     * Send a message to a user or group
     *
     * @param $from
     * @param $message
     * @param  null  $to
     * @param  bool  $isGroup
     * @param  null  $parameters
     *
     * @return false|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function sendMessage(
        $from,
        $message,
        $to,
        $isGroup = false,
        $parameters = null
    ) {
        self::$operation = 'message';
        $parameters['from'] = $from;
        ($isGroup) ? $parameters['to'] = $to : $parameters['gid'] = $to;
        $parameters['msg'] = $message;

        $result = self::sendRequest($parameters);

        return $result ? $result->members : $result;
    }

    /**
     * Send an API request to Mesibo API
     *
     * @param $parameters
     *
     * @return \Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private static function sendRequest(&$parameters)
    {
        $parameters['op'] = self::$operation;
        $parameters['token'] = self::$appToken;

        return self::getApiResponse($parameters);
    }

    /**
     * Retrieve the response from the Mesibo API
     *
     * @param $parameters
     *
     * @return false|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private static function getApiResponse(&$parameters)
    {
        try {
            $client = new Client();

            $response = $client->request('GET', self::$apiUrl, [
                'query' => $parameters,
            ]);

            $result = json_decode($response->getBody());
            if ($result && $result->result === true) {
                return $result;
            }

            return false;
        } catch (RequestException $e) {
            echo Message::toString($e->getRequest());
            if ($e->hasResponse()) {
                echo Message::toString($e->getResponse());
            }
        }
    }
}
